up:
	docker-compose up -d

down:
	docker-compose down
build:
	docker-compose build

migrate:
	php artisan migrate:refresh --seed

bash:
	docker exec -it react-php bash




